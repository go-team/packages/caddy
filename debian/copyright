Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: caddy
Upstream-Contact: https://github.com/caddyserver/caddy/issues
Source: https://github.com/caddyserver/caddy

Files: *
Copyright: 2015 Matthew Holt and The Caddy Authors
License: Apache-2.0

Files: caddyconfig/caddyfile/parse.go
       caddyconfig/caddyfile/lexer.go
       caddyconfig/caddyfile/lexer_test.go
       caddyconfig/caddyfile/parse_test.go
Copyright: 2015 Light Code Labs, LLC
License: Apache-2.0

Files: modules/caddyhttp/reverseproxy/fastcgi/client.go
Copyright: 2012 Junqing Tan <ivan@mysqlab.net> and The Go Authors
License: Apache-2.0

Files: modules/caddyhttp/encode/encode.go
Copyright: 2015 Matthew Holt and The Caddy Authors
           2015 The Httpgzip Authors
License: Apache-2.0
Comment: The initial enhancements related to Accept-Encoding, minimum content
 length, and buffer/writer pools were adapted from
 https://github.com/xi2/httpgzip then modified heavily to accommodate modular
 encoders and fix bugs.

Files: modules/caddyhttp/reverseproxy/streaming.go
Copyright: 2015 Matthew Holt and The Caddy Authors
           2011 The Go Authors
License: Apache-2.0 and BSD-3-Clause
Comment: Most of the code in this file was initially borrowed from the Go
 standard library and modified, which is attributed to "The Go Authors"

Files: dist/*
Copyright: 2015 Matthew Holt and The Caddy Authors
License: Apache-2.0
Comment: Repo included as merged orig-file, see debian/watch

Files: debian/*
Copyright: 2021 Peymaneh <peymaneh@posteo.net>
License: Apache-2.0
Comment: Debian packaging is licensed under the same terms as upstream

Files: debian/missing-sources/caddy/*
Copyright: 2015 Matthew Holt and The Caddy Authors
License: Apache-2.0
Comment: Bad formatting would have lintian throw very-long-line & source-is-missing

Files: debian/missing-sources/github.com/google/cel-go/*
Copyright: 2018-2021 Google LLC
License: Apache-2.0
Comment: Copied from Debian package golang-github-google-cel-go-dev (0.12.5+ds-1)

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
